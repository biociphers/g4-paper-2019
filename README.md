# g4-paper-2019
David S.M. Lee

Code and processed data for paper in submission assessing evidence for negative selection in gnomAD over UTR G-quadruplex forming sequences and enrichment for functional associations using data from ENCODE and GTEx.

The analysis is broadly separated into three major parts:  
1. Population genetics analysis of canonical, putative G-quadruplex (pG4) forming sequences within annotated UTRs  
2. Testing for enrichment of functional associations, including cis-eQTLs mapped by GTEx, and RNA-protein binding sites mapped by ENCODE  
3. Testing for enrichment of disease-associated variation in ClinVar, and performing an analysis of common variants in GTEx affecting UTR pG4 sequences that exhibit evidence of allelic imbalance.

## Folders
`scripts`: contains all of the analysis scripts used to generate main figures of the paper  
`RData`: contains processed RData objects used to generate figures  
`external_data`: contains processed, publicly available data organized by source  

## Reproducing the analyses
All of the code used to generate all of the figures are in `scripts`. Some of the requisite data for the functional enrichment analysis shown in Figures 3 and 4 are too large to be included, however, instructions are provided on how this data may be accessed publicly from GTEx/ENCODE within their respective directories under `external_data`.

This paper has been posted in biorXiv and this repository will continuously updated / revised as necessary.

## Accessing the Source Data
Source data used to generate each of the primary and supplemental Figures are accessible at: https://drive.google.com/file/d/10NpsVvMqnPcnLConWKSzF69EDsb-TnQ0/view?usp=sharing
>>>>>>> b79a58db18d96d25ee6a01f6074a4acfd79998de
