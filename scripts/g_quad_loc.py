#!/bin/bash python3

import pandas as pd
from itertools import groupby
from operator import itemgetter

# Purpose: identify genomic coordinates of G-quadruplex structures in mRNA within 5'UTR or 3'UTR

# First: establish genomic coordinates of 5'UTR and 3'UTR sequences of the RNA

# Read in the transcript start-stop mapping table

def start_stop_mapping(start_stop_mapping_table):

    with open(start_stop_mapping_table) as f:
        next(f) # skip the header line
        transcript_loc_dict = {}
        for line in f:
            row = line.split()
            transcript_id = row[4]
            if int(row[3]) < 0:
                utr_start_end = (int(row[2]), int(row[1])) # for (-) stranded UTRs
            else:
                utr_start_end = (int(row[1]), int(row[2]))
            if transcript_id in transcript_loc_dict.keys():
                transcript_loc_dict[transcript_id].append(utr_start_end)
            else:
                transcript_loc_dict[transcript_id] = [utr_start_end]

    return transcript_loc_dict



def generate_utr_genomic_locs(tx_coordinates):

    """
    The purpose of this function is to take a list of start-stop genomic coordinates
    and concatenate them into a continuous range of genomic locations.
    Args:
        tx_coordinates: a list of tuples corresponding to genomic start-stop coordinates
    Returns:
        genomic_loc: a list of genomic locations corresponding to each position within
        the origin start-stop coordinates
    """

    # determine if the sequence is (-) strand or (+) strand

    if tx_coordinates[0][1] > tx_coordinates[0][0]:
        step = 1  # For (+) strand sequence
    else:
        step = -1  # For (-) strand sequence

    # generate the concatenated sequence of genomic coordinates

    genomic_loc = []

    for coordinates in tx_coordinates:
        start = coordinates[0]
        stop = coordinates[1] + step
        genomic_loc += list(range(start, stop, step))

    if step == 1:
        return sorted(genomic_loc)
    else:
        return sorted(genomic_loc, reverse=True)

def format_genomic_ranges(coordinates_list):
    """
    The purpose of this function is to generate a string of genomic ranges from
    :param coordinates_list:
    :return: a list of genomic ranges formatted as ['loc_start:loc_end'] if the coordinates are continuous. If the
    coordinates do not span a continuous range a list of ranges is returned: e.g. ['3372683:3372692', '3374725:3374741']
    """

    coordinates_list = sorted(coordinates_list)

    str_list = []

    for k, g in groupby(enumerate(coordinates_list), lambda ix: ix[0] - ix[1]):
        ilist = list(map(itemgetter(1), g))
        if len(ilist) > 1:
            str_list.append('%d:%d' % (ilist[0], ilist[-1]))
        else:
            str_list.append('%d' % ilist[0])

    return str_list

def find_gquad_genomic_coordinates(utr_table, utr_genomic_locs):

    """
    The purpose of this function is to match relative g-quad locations in UTRs to the absolute gene locations within the
    genome coordinates in utr_genomic_locs.
    :param utr_table: A pandas dataframe with ensemble transcript ID, and the relative location of each g-quadruplex
    start motif within the UTR sequence associated with the ensembl transcript ID
    :param utr_genomic_locs: a dictionary associating ensembl transcript UTRs with the genomic locations that they span
    :return:
    """

    gquad_genomic_coords = {}
    # gquad_genomic_ids = {}  # This is to return the outermost genomic coordinates of the g-quad

    skipped = []

    # iterate over rows of utr_table to generate

    for index, row in utr_table.iterrows():
        transcript_id = row['ensembl_transcript_id']
        utr_g4_start = int(row['utr_gquad_start'])
        utr_g4_length = int(row['gquad_length'])
        if utr_g4_start == -1:
            try:
                gquad_genomic_coords[transcript_id] = [-1]  # transcript has no gquad
            except:
                skipped.append(transcript_id)
        else:
            utr_g4_start -= 1  # adjust for python 0 indexing
            try:
                genomic_locs = generate_utr_genomic_locs(utr_genomic_locs[transcript_id])
                g4_coordinates = genomic_locs[utr_g4_start: utr_g4_start + utr_g4_length]

                # mark start and stop genomic locations for each continuous g-quad segment

                # check if the transcript is already in gquad_genomic_coords for multiple g-quads
                if transcript_id in gquad_genomic_coords.keys():
                    gquad_genomic_coords[transcript_id].append(format_genomic_ranges(g4_coordinates))
                else:
                    gquad_genomic_coords[transcript_id] = [format_genomic_ranges(g4_coordinates)]
                # gquad_genomic_coords[transcript_id] = format_genomic_ranges(g4_coordinates)
                # gquad_genomic_ids[transcript_id] = g4_genomic_id
            except:
                skipped.append(transcript_id)

    print('skipped transcripts:\n')
    print(set(skipped))

    return gquad_genomic_coords


def write_g4_locs(gquad_genomic_coords, write = True, outdir = './g4_locs.txt'):

    """

    :param gquad_genomic_coords:
    :return:
    """

    start = []
    end = []
    transcript_id = []
    # g4_coords = []  : this might not be useful to record since it is just start:end
    splice = []

    for transcript in gquad_genomic_coords.keys():
        for g4 in gquad_genomic_coords[transcript]:
            if g4 == -1:
                pass
            else:
                #splice.append(len(gquad_genomic_coords[transcript]) > 1)  # record if g4 spans splice junction
                for coord in g4:
                    splice.append(len(g4) > 1)  # record if the g4 spans a splice junction
                    start_stop = [int(x) for x in coord.split(':')]
                    #start_stop = [int(x) for x in g4.split(':')]
                    if len(start_stop) == 1:
                        start.append(start_stop[0])
                        end.append(start_stop[0])
                    else:
                        start.append(start_stop[0])
                        end.append(start_stop[1])
                    transcript_id.append(transcript)
                    # g4_coords.append(g4)

    g4_genomic_coordinates = pd.DataFrame(
        {'g4_start': start,
         'g4_end': end,
         'ensembl_transcript_id': transcript_id,
         'splice_junction': splice
         }
    )

    if write:
        g4_genomic_coordinates.to_csv(outdir, sep=' ')
        print('results written to file')
    else:
        return g4_genomic_coordinates


start_stop = start_stop_mapping('./RData/utr5_start_stop.txt')
utr = pd.read_table('./RData/utr5_pc_chr.txt', sep = ' ')

utr5_res = find_gquad_genomic_coordinates(utr, start_stop)
write_g4_locs(utr5_res, write=True, outdir='./RData/utr5_g4_locs_mult.txt')

start_stop = start_stop_mapping('./RData/utr3_start_stop.txt')
utr = pd.read_table('./RData/utr3_pc_chr.txt', sep = ' ')

utr3_res = find_gquad_genomic_coordinates(utr, start_stop)
write_g4_locs(utr3_res, write=True, outdir='./RData/utr3_g4_locs_mult.txt')

# Repeat for UTR G-Runs:

start_stop = start_stop_mapping('./RData/utr5_start_stop.txt')
utr = pd.read_table('./RData/utr5_pc_gruns.txt', sep=' ')
utr5_res = find_gquad_genomic_coordinates(utr, start_stop)
write_g4_locs(utr5_res, write=True, outdir='./RData/utr5_g4_locs_gruns.txt')

start_stop = start_stop_mapping('./RData/utr3_start_stop.txt')
utr = pd.read_table('./RData/utr3_pc_gruns.txt', sep=' ')
utr3_res = find_gquad_genomic_coordinates(utr, start_stop)
write_g4_locs(utr3_res, write=True, outdir='./RData/utr3_g4_locs_gruns.txt')

# Repeat for almost UTR G4s
start_stop = start_stop_mapping('./RData/utr5_start_stop.txt')
utr = pd.read_table('./RData/utr5_almost_g4.txt', sep=' ')
utr5_res = find_gquad_genomic_coordinates(utr, start_stop)
write_g4_locs(utr5_res, write=True, outdir='./RData/utr5_almost_g4_locs.txt')

# Repeat for almost UTR G4s
start_stop = start_stop_mapping('./RData/utr3_start_stop.txt')
utr = pd.read_table('./RData/utr3_almost_g4.txt', sep=' ')
utr3_res = find_gquad_genomic_coordinates(utr, start_stop)
write_g4_locs(utr3_res, write=True, outdir='./RData/utr3_almost_g4_locs.txt')