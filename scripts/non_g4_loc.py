import pandas as pd
import re
from itertools import groupby
from operator import itemgetter

def generate_utr_genomic_locs(tx_coordinates):

    """
    The purpose of this function is to take a list of start-stop genomic coordinates
    and concatenate them into a continuous range of genomic locations.
    Args:
        tx_coordinates: a list of tuples corresponding to genomic start-stop coordinates
    Returns:
        genomic_loc: a list of genomic locations corresponding to each position within
        the origin start-stop coordinates
    """

    # determine if the sequence is (-) strand or (+) strand

    if tx_coordinates[0][1] > tx_coordinates[0][0]:
        step = 1  # For (+) strand sequence
    else:
        step = -1  # For (-) strand sequence

    # generate the concatenated sequence of genomic coordinates

    genomic_loc = []

    for coordinates in tx_coordinates:
        start = coordinates[0]
        stop = coordinates[1] + step
        genomic_loc += list(range(start, stop, step))

    if step == 1:
        return sorted(genomic_loc)
    else:
        return sorted(genomic_loc, reverse=True)

def format_genomic_ranges(coordinates_list):
    """
    The purpose of this function is to generate a string of genomic ranges from
    :param coordinates_list:
    :return:
    """

    coordinates_list = sorted(coordinates_list)

    str_list = []

    for k, g in groupby(enumerate(coordinates_list), lambda ix: ix[0] - ix[1]):
        ilist = list(map(itemgetter(1), g))
        if len(ilist) > 1:
            str_list.append('%d:%d' % (ilist[0], ilist[-1]))
        else:
            str_list.append('%d' % ilist[0])

    return str_list


def start_stop_mapping(start_stop_mapping_table):

    with open(start_stop_mapping_table) as f:
        next(f) # skip the header line
        transcript_loc_dict = {}
        for line in f:
            row = line.split()
            transcript_id = row[4]
            if int(row[3]) < 0:
                utr_start_end = (int(row[2]), int(row[1])) # for (-) stranded UTRs
            else:
                utr_start_end = (int(row[1]), int(row[2]))
            if transcript_id in transcript_loc_dict.keys():
                transcript_loc_dict[transcript_id].append(utr_start_end)
            else:
                transcript_loc_dict[transcript_id] = [utr_start_end]

    return transcript_loc_dict


def find_g_runs(utr_table, utr_genomic_locs, outdir, write = True):

    g_run_genomic_coords = {}
    seen = []
    chr = []
    start = []
    stop = []

    for index, row in utr_table.iterrows():
        transcript_id = row['ensembl_transcript_id']
        utr_seq = row[0]
        utr_chr = row['chromosome']
        g_runs = [m.start() for m in re.finditer('(?:G{3})', utr_seq)]
        if len(g_runs) < 1:
            pass
        else:
            try:
                genomic_locs = generate_utr_genomic_locs(utr_genomic_locs[transcript_id])
                for g_run_start in g_runs:
                    g_run_coords = genomic_locs[g_run_start: g_run_start + 3]
                    g_run_coords = format_genomic_ranges(g_run_coords)
                    for coordinates in g_run_coords:
                        g_loc = str(utr_chr) + ':' + coordinates
                        if g_loc in seen:
                            pass
                        else:
                            seen.append(g_loc)
                            start_stop = [int(x) for x in coordinates.split(':')]
                            if len(start_stop) == 1:
                                chr.append(utr_chr)
                                start.append(start_stop[0])
                                stop.append(start_stop[0])
                            else:
                                chr.append(utr_chr)
                                start.append(start_stop[0])
                                stop.append(start_stop[1])
            except KeyError:
                print('{} not found in utr genomic locations table and is being skipped'.format(transcript_id))

    g_run_df = pd.DataFrame(
        {'chromosome': chr,
         'start': start,
         'end': stop}
    )

    if write:
        g_run_df.to_csv(outdir, sep=' ')
        print('results written to file')
    else:
        return g_run_df

start_stop = start_stop_mapping('./RData/utr5_start_stop.txt')

test_nong4 = pd.read_table('./RData/utr5_pc_chr.txt', sep = ' ')

test_result = find_g_runs(test_nong4, start_stop, outdir = './utr5_grun_out.txt')

start_stop = start_stop_mapping('./RData/utr3_start_stop.txt')
utr3_nong4 = pd.read_table('./RData/utr3_pc_chr.txt', sep=' ')
utr3_result = find_g_runs(utr3_nong4, start_stop, outdir='./utr3_grun_out.txt')


"""
        
                    start_stop = [int(x) for x in coordinates.split(':')]
                    
                    if len(start_stop) == 1:
                    
                if ''.join(g_run_coords) in seen:
                    pass
                else:
                    seen.append(''.join(g_run_coords))
                    chr.append(utr_chr)
                    start.append()
        genomic_locs = generate_utr_genomic_locs(utr_genomic_locs)
        for g_run in g_runs:
            genomic_locs = generate_utr_genomic_locs(utr_genomic_locs[transcript_id])
"""








"""
    for index, row in utr_table.iterrows():
        transcript_id = row['ensembl_transcript_id']
        utr_seq = row[0]
        for i in range(len(utr_seq) - (chunk - 1)):
            seq = utr_seq[i:i+chunk]
            if collections.Counter(seq)['G'] >= runs:
"""

