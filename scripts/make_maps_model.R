# Make MAPS Model
# David Lee September 2019

# This script is used to generate a mutability-adjusted proportion of
# singletons model. Based off of code by PJ short:
# https://github.com/pjshort/dddMAPS
# Running this script produces a ps_lm.RData object that is used for downstream
# MAPS analyses.

############# 
## Set env ##
#############
setwd('~/Documents/g4-paper-2019/')

library(dplyr)
library(GenomicRanges)
source('scripts/generate_data.R')

load('external_data/methylation/methyl_means.RData')
canonical <- data.table::fread('external_data/constraint/ensembl_known.txt', header = FALSE)

granges_from_vcf <- function(vcf) {
  
  vcf$end <- vcf$POS
  grange <- makeGRangesFromDataFrame(vcf, keep.extra.columns = TRUE,
                                     ignore.strand = TRUE,
                                     seqnames.field = "CHROM",
                                     start.field = "POS",
                                     end.field = "end")
  
  return(grange)
  
}

##############################
## Make MAPS model function ##
##############################

make_ps_model <- function(cds_vcf, mu, AC_thresh = 1) {
  
  singletons <- cds_vcf %>%
    filter(AC == AC_thresh)
  
  singletons_context <- table(singletons$mut)
  context_n <- table(cds_vcf$mut)
  
  prop_singletons <- data.frame('mutation' = names(singletons_context),
                                'singletons' = c(singletons_context))
  prop_n <- data.frame('mutation' = names(context_n),
                       'n' = c(context_n))
  
  prop_singletons <- left_join(prop_singletons, prop_n, by ='mutation')
  
  prop_singletons <- left_join(prop_singletons, mut, by = 'mutation')
  ps_lm <- lm(singletons/n ~ mu_snp, data = prop_singletons, weights = n)
  
  return(ps_lm)
  
}

make_ps2_model <- function(cds_vcf, mu, AC_thresh = 1) {
  
  
  variants <- split(cds_vcf, cds_vcf$group)
  
  ps_models <- lapply(variants, function(x) make_ps_model(x, mu, AC_thresh))
  
  return(ps_models)
  
  
}



annotate_methylation <- function(variants, mutation_table, methylation) {
  
  variants <- left_join(variants, methylation)
  
  # set missing methylation values to zero
  variants$methyl_bins[is.na(variants$methyl_bins)] <- 0
  
  fixed_context <- ifelse(variants$context %in% mutation_table$context,
                          variants$context,
                          chartr('ATGC','TACG',variants$context))
  fixed_mut <- ifelse(variants$context %in% mutation_table$context,
                      variants$mut_context,
                      chartr('ATGC','TACG',variants$mut_context))
  
  # amend mutation context for MAPS score calculation
  variants$mut <- paste0(fixed_context,':',
                         variants$methyl_bins,
                         '>',
                         fixed_mut)
  
  variants$mut <- ifelse(variants$mut %in% mutation_table$mutation,
                         variants$mut,
                         gsub('[1|2]',
                              '0',
                              variants$mut))
  
  variants$context <- fixed_context
  variants$mut_context <- fixed_mut
  
  # annotate variants for methylation adjustment
  var_adjust <- mutation_table %>%
    filter(methylation_level != 0) %>%
    select(context) %>% unlist()
  
  variants$group <- ifelse((variants$context %in% var_adjust),
                           ifelse((variants$REF == 'C' & variants$ALT == 'T') | (variants$REF == 'G' & variants$ALT == 'A'),
                                  'methyl_adjust',
                                  'no_adjust'),
                           'no_adjust')
  
  return(variants)
  
}

###########################
## Making the MAPS model ##
###########################

# load and filter synonymous variants from CDS in gnomAD

cds <- data.table::fread("external_data/gnomad/genomes_cds_out/vars/combined_synonymous_filtered.txt", sep = '\t', header = FALSE,
                         col.names = c('CHROM','POS','REF','ALT','rf_tp_probability',
                                       'AN','AC','AF','lcr','decoy','segdup','vep_canonical'))
seq <- data.table::fread('external_data/gnomad/genomes_cds_out/vars/combined_synonymous_filtered.sequences', header = FALSE)
cds$heptamer <- toupper(seq$V2)

cds <- cds %>%
  filter(AC != 0, CHROM != "X", AN >=  25132)

cds$context <- substr(cds$heptamer, start = 3, stop = 5)

colnames(cds) <- c('CHROM','POS','REF','ALT','RF_TP_PROB','AN','AC',
                            'AF','AF_popmax','ctrl_AC','ctrl_AN',
                            'nhomalt_popmax','lcr','decoy','segdup',
                            'syn_transcript','context','heptamer')

cds$mut_context <- paste0(substr(cds$context, 1, 1),
                              cds$ALT,
                              substr(cds$context, 3, 3))

mut <- data.table::fread('external_data/mutation_rate_methylation_bins.txt')

mut <- mut %>%
  mutate(ref_context = paste0(context, ':', methylation_level),
         mut_context = paste0(substr(context, 1, 1),
                              alt,
                              substr(context, 3, 3)),
         mutation = paste0(ref_context, '>', mut_context))

# annotate methylation
cds <- annotate_methylation(cds, mut, combined_methylation_estimates)
cds$methyl_bins <- as.numeric(as.character(cds$methyl_bins))


# make the ps_lm model:
ps_lm_genomes <- make_ps2_model(cds, mut, AC_thresh = 1)

if (!dir.exists('RData/MAPS')) {
  dir.create('RData/MAPS')
}

save(ps_lm_genomes, file = 'RData/MAPS/ps_lm_genomes.RData')