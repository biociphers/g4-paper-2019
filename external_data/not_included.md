# g4-paper-2019
David S.M. Lee

## Data not included in this repo:  
Background GTEx SNPs used for cis-eQTL enrichment analyses (Fig. 3) are not included in this repo due to their size. Background SNP sets for the v7 and v6p release can be obtained directly from GTEx here: https://gtexportal.org/home/datasets. Additionally GTEx transcript TPM quantifications used to generate results for Fig. 2b can be obtained from the GTEx v7 release RNAseq data from the file "GTEx_Analysis_2016-01-15_v7_RSEMv1.2.22_transcript_tpm.txt.gz".

