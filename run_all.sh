#!/bin/bash

# This script will run R-scripts in the directory scripts to regenerate figures from the paper.
# Since file paths may be different, the working directory in each Rscript may need to be changed before this script is run.


# First generate the MAPS model
Rscript scripts/make_maps_model.R

# Generate Figure 1b
Rscript scripts/Figure_1b.R

# Generate Figure 1c
Rscript scripts/Figure_1c.R

# Generate Figure 1d
Rscript scripts/Figure_1d.R

# Generate Figure 2
Rscript scripts/Figure2.R

# Generate Figures 3 and 4
Rscript scripts/Figure_3_4.R

# Generate Figure 5
Rscript scripts/Figurei_5.R

# Generate supplementary Figure 4:
Rscript scripts/sup_fig_4.R
